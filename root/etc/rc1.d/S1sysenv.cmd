set SystemDrive=%~d0
set SystemRoot=%~d0

set HOMEDRIVE=%home%
set PUBLIC=%HOMEDRIVE%\home\public
set OLDTMP=%TEMP%
set TEMP=%SystemDrive%\tmp
set TMP=%TEMP%
set ALLUSERSPROFILE=%SystemDrive%\etc
set ProgramFiles=%opt%
set ProgramFiles(x86)=%opt%
set ProgramW6432=%opt%
set CommonProgramFiles=%opt%\common
set CommonProgramFiles(x86)=%opt%\common
set CommonProgramW6432=%opt%\common
set ProgramData=%SystemDrive%\var\opt
set pathext=%pathext%;.LNK