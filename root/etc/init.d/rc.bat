@echo off
REM for /F "usebackq delims=" %%A in (`\bin\ls -1 \etc\rc%1.d`) do "\etc\rc%1.d\%%A"

set RC_ACT=s
if "%2"=="k" set RC_ACT=k
if "%2"=="kill" set RC_ACT=k
if "%2"=="K" set RC_ACT=k
if "%2"=="KILL" set RC_ACT=k
if "%2"=="stop" set RC_ACT=k
if "%2"=="STOP" set RC_ACT=k

if "%RC_ACT%"=="s" %info% Entering runlevel %1
if "%RC_ACT%"=="k" %info% Exiting runlevel %1

for /F "usebackq delims=" %%A in (`\bin\find \etc\rc%1.d -iregex "\\etc\\rc%1\.d\\%RC_ACT%[0-9]+.+\.\(bat\|cmd\)"`) do @"%%A"
