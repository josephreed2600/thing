set pathbak=%path%
path %SYSTEMDRIVE%\bin;%SYSTEMDRIVE%\sbin;%SYSTEMDRIVE%\usr\bin;%SYSTEMDRIVE%\usr\sbin;%opt%\bin;%SYSTEMDRIVE%\lib;%SYSTEMDRIVE%\usr\lib

which cat.exe
which tr.exe
which sed.exe
which dos2unix.exe

if exist \var\cache\optpaths (
 cat \var\cache\optpaths | tr ; \n | sed "s#^.:\\#%PROGRAMFILES%\\#" | \usr\bin\dos2unix | tr \n ; > \var\cache\optpaths.tmp
 del \var\cache\optpaths
 rename \var\cache\optpaths.tmp optpaths
) else (call \usr\bin\pathupdater)

if exist \var\cache\paths (
 cat \var\cache\paths | tr ; \n | sed "s#^.:\\#%SYSTEMDRIVE%\\#" | \usr\bin\dos2unix | tr \n ; > \var\cache\paths.tmp
 del \var\cache\paths
 rename \var\cache\paths.tmp paths
) else (call \usr\bin\pathupdater)

for /F "usebackq delims=" %%A in (`\bin\cat \var\cache\optpaths`) do set optpath=%%A
for /F "usebackq delims=" %%A in (`\bin\cat \var\cache\paths`) do set rootpath=%%A
set path=%rootpath%%optpath%%pathbak%
REM set path=bin;..\bin;lib;..\lib;%path%%pathbak%
