@echo off
:begin

:prompt
set /P username=username: 
if "%username%"=="" goto prompt
if not exist "%HOMEDRIVE%\%username%" (
 echo [error] Home directory for user `%username%' not found ^(%HOMEDRIVE%\%username%^)
 goto prompt
)

:fail

:success

:eof