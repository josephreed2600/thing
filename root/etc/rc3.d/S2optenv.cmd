REM Editor's note:
REM These lines have been commented because the paths they point to have been censored from the repo, since they are not integral
REM parts of the project. A user could add them back in and uncomment these lines to have them set.
@echo off
REM set JAVA_HOME=%opt%\jdk-8u111-windows-amd64
REM set PYTHONHOME=%opt%\python27
REM for /F %%A in (\etc\init.d\pythonpath.path) do set PYTHONPATH=%%A
REM set TESSDATA_PREFIX=%ProgramFiles%\TesseractOCR
