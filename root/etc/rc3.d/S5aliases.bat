@echo off
rem doskey alias=doskey $* $^^*
call prints

doskey update-path=echo N ^| pathupdate $*

call alias gimp gimp-2.8
call alias npp notepad++
call alias logo logo32
call alias np notepad
call alias notpead notepad
call alias eixt exit
call alias tess tesseract
call alias eject %systemdrive%\bin\um.bat