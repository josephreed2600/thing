REM @echo off
REM echo [info] Searching for GUID %1 to mount as %2
set targetguid=%1
set mountpoint=%2

for %%A in (D;E;F;G;H;I;J;K;L;M;N;O;P;Q;R;S;T;U;V;W;X;Y;Z) do (
REM echo [info]  Trying drive %%A:...
 if exist %%A:\.partid (
REM echo [info] %%A:\.partid found!
  for /F %%a in (%%A:\.partid) do (
REM echo [info] %%A:\.partid contents read: %%a
  if "%%a"=="%targetguid%" (
REM  echo [info] Found GUID matches target GUID!
   %info% Setting %mountpoint%=%%A:
   set %mountpoint%=%%A:
   goto found
   )
  )
 )
)
:found
REM @echo on