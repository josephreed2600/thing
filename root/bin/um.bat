%~d0
set mount=%~d0\bin\mount-guid
set guidhome=367a8af8-0473-471a-94cf-e36bf8c6be55
call %mount% %guidhome% home
set guidopt=766f31ef-cca3-4774-ab6c-4e75b523aa73
call %mount% %guidopt% opt
set guidsw=21595a70-c44a-4769-a209-b9d1d1165de7
call %mount% sw
REM To add more drives to unmount, add the guid/mount command above and the variable name below

for %%A in (%home% %opt% %sw%) do (
echo rmdrv %%A
%~d0\bin\umount %%A
)

echo on
if defined oldtmp (
set temp=%oldtmp%
set tmp=%oldtmp%
)
C:

%~d0\bin\umount %~d0

REM if not exist %temp% (
REM set newtemp=1
REM set tempfile=f6d15257-ed88-47cb-add6-324a6c37c3d5.bat
REM md %temp%
REM )
REM cd %temp%
REM %~d0\bin\cp %~d0\bin\umount.exe umount.exe
REM if "%newtemp%"=="1" (
REM echo umount %~d0 -s ^&^& cd .. ^&^& start /B rmdir %temp% /S /Q ^& pause>%tempfile%
REM %tempfile%
REM ) else (
REM umount %~d0 -s
REM )