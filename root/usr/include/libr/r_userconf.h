#ifndef _INCLUDE_R_CONFIGURE_H_
#define _INCLUDE_R_CONFIGURE_H_

#ifdef LIL_ENDIAN
#undef LIL_ENDIAN
#endif
#define LIL_ENDIAN 1
#define CPU_ENDIAN 0

#define DEBUGGER 1

#if __WIN32__ || __CYGWIN__ || MINGW32
#define R2_PREFIX "."
#define R2_LIBDIR "./lib"
#define R2_INCDIR "./include/libr"
#define R2_DATDIR "./share"
#else
#define R2_PREFIX "/usr/local"
#define R2_LIBDIR "/usr/local/lib"
#define R2_INCDIR "/usr/local/include/libr"
#define R2_DATDIR "/usr/local/share"
#endif

#define R2_VERSION "0.9.6"
#define HAVE_LIB_MAGIC 0
#define USE_LIB_MAGIC 0
#define HAVE_LIB_SSL 0
#define HAVE_LIB_EWF 0

#define R2_WWWROOT "/usr/local/share/radare2/0.9.6/www"

#endif
