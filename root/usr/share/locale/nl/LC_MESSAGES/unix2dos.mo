��          �      ,      �  &   �     �  �  �  +   �  "   �  '   �  /     :   2  O   m  /   �  &   �  1     )   F  4   p  ;   �      �  |    (        �    �  .   �  /     1   8  6   j  D   �  M   �  6   4  1   k  A   �  5   �  7     N   M  2   �                   	                                
                                     output file remains in '%s'
 With native language support.
 unix2dos %s (%s)
Usage: unix2dos [-fhkLqV] [-c convmode] [-o file ...] [-n infile outfile ...]
 -c --convmode    conversion mode
   convmode       ASCII, 7bit, ISO, default to ASCII
 -f --force       force conversion of all files
 -h --help        give this help
 -k --keepdate    keep output file date
 -L --license     print software license
 -n --newfile     write to new file
   infile         original file in new file mode
   outfile        output file in new file mode
 -o --oldfile     write to old file
   file ...       files to convert in old file mode
 -q --quiet       quiet mode, suppress all warnings
                  always on in stdio mode
 -V --version     display version number
 unix2dos: Skipping %s, not a regular file.
 unix2dos: Skipping binary file %s
 unix2dos: can not write to output file
 unix2dos: converting file %s to DOS format ...
 unix2dos: converting file %s to file %s in DOS format ...
 unix2dos: error: Value of environment variable UNIX2DOS_LOCALEDIR is too long.
 unix2dos: invalid %s conversion mode specified
 unix2dos: problems converting file %s
 unix2dos: problems converting file %s to file %s
 unix2dos: problems renaming '%s' to '%s'
 unix2dos: program error, invalid conversion mode %d
 unix2dos: target of file %s not specified in new file mode
 unix2dos: using %s as temp file
 Project-Id-Version: unix2dos 4.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-01-24 08:35+0100
PO-Revision-Date: 2009-12-09 17:20+0100
Last-Translator: Erwin Waterlander <waterlan@xs4all.nl>
Language-Team: Dutch <vertaling@vrijschrift.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
           uitvoerbestand blijft in '%s'
 Met moedertaal ondersteuning.
 unix2dos %s (%s)
Usage: unix2dos [-fhkLqV] [-c convmode] [-o file ...] [-n infile outfile ...]
 -c --convmode    conversie modus
   convmode       ASCII, 7bit, ISO, standaard is ASCII
 -f --force       forceer conversie van alle bestanden
 -h --help        print deze help tekst
 -k --keepdate    bewaar datum uitvoerbestand
 -L --license     print software licentie
 -n --newfile     schrijf naar nieuw bestand
   infile         origineel bestand in 'nieuw bestand modus'
   outfile        uitvoerbestand in 'nieuw bestand modus'
 -o --oldfile     overschrijf oud bestand
   file ...       te converteren bestanden in 'oud bestand modus'
 -q --quiet       stille werking, onderdruk alle waarschuwingen
                  altijd aan in stdio modus
 -V --version     print versie nummer
 unix2dos: Overslaan %s, geen normaal bestand.
 unix2dos: Binair bestand %s wordt overgeslagen
 unix2dos: kan niet schrijven naar uitvoerbestand
 unix2dos: converteren bestand %s naar DOS formaat ...
 unix2dos: converteren bestand %s naar bestand %s in DOS formaat ...
 unix2dos: fout: Waarde van omgevingsvariabele UNIX2DOS_LOCALEDIR is te lang.
 unix2dos: ongeldige %s conversie modus gespecificeerd
 unix2dos: problemen met conversie van bestand %s
 unix2dos: problemen met conversie van bestand %s naar bestand %s
 unix2dos: problemen met hernoemen van '%s' naar '%s'
 unix2dos: programma fout, ongeldige conversie modus %d
 unix2dos: doel van bestand %s is niet gespecificeerd in 'nieuw bestand modus'
 unix2dos: %s wordt als tijdelijk bestand gebruikt
 