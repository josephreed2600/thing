
File list

    README.txt          : This file.
    INSTALL.txt         : How to build and install
    COPYING.txt         : distribution license.
    unix2dos.txt        : Manual, text format.
    unix2dos.ps         : Manual, PostScript format.
    unix2dos.pdf        : Manual, PDF format.
    ChangeLog.txt       : Change log.
    TODO.txt            : Things to do.


unix2dos - UNIX to DOS text file format converter.

Get also the companion package 'dos2unix' for conversion from DOS to Unix.


Project page: http://www.xs4all.nl/~waterlan/dos2unix.html

SourceForge page: http://sourceforge.net/projects/dos2unix/


This is an update of Benjamin Lin's implementations of dos2unix and unix2dos.
Benjamin Lin's implementations of dos2unix and unix2dos are part of all RedHat
based Linux distributions and others. This update includes all RedHat patches
and fixes several other problems. Internationalization has been added and ports
to DOS, Windows, Cygwin and OS/2 Warp have been made. 

These implementations of dos2unix and unix2dos were originally made as an Open
Source alternative for dos2unix/unix2dos under SunOS/Solaris. They have a few
similar conversion modes, namely ascii, 7bit and iso. The first versions were
made by John Birchfield in 1989, and in 1995 rewritten from scratch by Benjamin
Lin. 


Erwin Waterlander
waterlan@xs4all.nl
http://www.xs4all.nl/~waterlan/

