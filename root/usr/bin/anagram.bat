@echo off
echo %*| for /f "usebackq delims=\ eol=\" %%A in (`sed "s/ /+/g"`) do (
echo %%A | sed "s/ //g"> anagram.tmp
)

set /p query=<anagram.tmp
set url="http://wordsmith.org/anagram/anagram.cgi?anagram=%query%&t=0&a=n"
explorer %url%
:cleanup
del anagram.tmp
set query=
:end
exit /B