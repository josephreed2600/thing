@echo off

for /F "usebackq delims=" %%A in (`echo %PATHEXT% ^| tr ";" "#" ^| tr [:upper:] [:lower:]`) do set A=%%A
for /F "usebackq delims=" %%B in (`echo %A% ^| tr [:lower:] [:upper:]`) do set B=%%B
for /F "usebackq delims=" %%C in (`echo %A%%B% ^| sed "s/ \./|\1/g;s/[. ]//g"`) do set EXT=%%C

for /F "usebackq" %%A in (`find %SYSTEMDRIVE%\opt -type d -regex "%SYSTEMDRIVE%\\opt\\.+\\bin" -maxdepth 2 ^| xargs -iP find P -type f ^| tr \/ \\`) do @echo %%~fA  | grep -Eie "Z:\\opt\\.+\\bin\\.+\.\(%EXT%\)"
