@echo off
if "%1"=="" (
echo.
echo Usage: %0 [01]
echo ^	0^	Load original registry configuration
echo ^	1^	Load modified registry configuration
echo.
exit /b 1
)


pushd %~dp0

if "%1"=="1" (
reg.exe export HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced \etc\original.reg
reg.exe import \etc\showext.reg
goto eof
)

if "%1"=="0" (
reg.exe import \etc\original.reg
goto eof
)

popd

:eof
exit /b 0