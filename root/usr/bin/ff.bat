@echo off
set format=%1
set input=%2
echo ffmpeg -i %input% %input:~0,-4%.%format%
ffmpeg -i %input% %input:~0,-4%.%format%
echo.
echo.
pause