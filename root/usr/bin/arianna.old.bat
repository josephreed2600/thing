@echo off

:askForHelp
if "%1"=="" goto help
if "%1"=="-h" goto help
if "%1"=="--help" goto help
if "%1"=="/?" goto help

:echoing
echo.
echo Downloading
echo %2
echo through %1 connections.
pause
echo.

:dl
echo.
aria2c %2 --file-allocation=none -c --split=%1 -x %1 -j %1
echo.
pause
exit /b


:help
echo Usage:
echo.
echo arianna ^<n^> ^<URL^>
echo.
echo ^ n        number of connections to use
echo ^ URL      URL to download
echo.
echo This aria defaults to:
echo --file-allocation=none --continue
echo.
echo arianna will not pre-allocate space for the file. It will attempt
echo to continue downloading the file if it has been started already.
pause
exit /b 0
