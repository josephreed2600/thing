@echo off
if "%1"=="" (
echo.
echo %0 target [directory [link name]]
echo.
exit /b 1
)
set TARGET=%~dpnx1
set EXT=%~x1
if "%EXT%"==".exe" set EXT=
if "%3"=="" (
set NAME=%~n1%EXT%.lnk
) else (
set NAME=%3.lnk
)
if "%2"=="" (
set DEST=%CD%\%NAME%
) else (
set DEST=%2\%NAME%
)

echo target=%target%
echo dest=%dest%

cscript \usr\bin\createShortcut.vbs "%target%" "%dest%"