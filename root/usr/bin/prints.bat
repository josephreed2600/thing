@echo off
doskey print=^<nul set /p =$*
doskey debug=echo [97m[debug][0m $*
doskey info=echo [96m[info][0m $*
doskey warn=echo [93m[warn][0m $*
doskey error=echo [91m[error][0m $*
doskey ok=echo [92m[ ok ][0m $*

set debug=echo [97m[debug][0m
set info=echo [96m[info][0m
set warn=echo [93m[warn][0m
set error=echo [91m[error][0m
set ok=echo [92m[ ok ][0m