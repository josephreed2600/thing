@echo off
if exist %~dpf1\bin (
  echo %~dpf1\bin;>>%systemdrive%\var\cache\optpaths
  path %~dpf1\bin;%path%
  if exist %~dpf1\lib (
    echo %~dp1\lib;>>%systemdrive%\var\cache\optpaths
    path %~dpf1\lib;%path%
)
if exist %~dpf1 (
  echo %~dpf1;>>%systemdrive%\var\cache\optpaths
  path %~dpf1;%path%
) else (
  goto err
)

cat %systemdrive%\var\cache\optpaths | tr -d \n\r 1> %systemdrive%\var\tmp\optpaths
rm %systemdrive%\var\cache\optpaths
mv %systemdrive%\var\tmp\optpaths %systemdrive%\var\cache

:exit
exit /b



:err
echo %0: path "%~dpf1" not found1>&2
goto exit