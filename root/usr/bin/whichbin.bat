@echo off

REM for /F "usebackq" %A in (`echo ^%PATHEXT^% ^| tr ";" "\n" `) do @echo foo%A

pclip > "%TEMP%\tmp_whichbin"
echo %PATHEXT% | tr ; \n | gclip
for /F %%A in ('pclip') do which -a %1%%A 2>nul
cat "%TEMP%\tmp_whichbin" | gclip
del "%TEMP%\tmp_whichbin"
