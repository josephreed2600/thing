@echo off
:iffy
if '%1'=='-q' goto q
if '%1'=='--quiet' goto q
if '%1'=='-c' goto q
if '%1'=='--copy' goto q
if '%1'=='-s' goto q
if '%1'=='--silent' goto q
if '%1'=='-d' goto curly
if '%1'=='--display' goto curly
if '%1'=='--show' goto curly

:curly
curl -s http://www.vpnbook.com/freevpn | grep -a Password | cut -c 27-34 | tail -n 1
goto ok

:q
curl -s http://www.vpnbook.com/freevpn | grep -a Password | cut -c 27-34 | tail -n 1 | clip
goto quit

:ok
@pause > nul
goto quit

:quit
exit /b
