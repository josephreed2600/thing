@echo off
echo [info] Wrapper initialized.
cmd /c "echo Y | pathupdater %* && exit /b %pathupdate%" 2>nul && goto done || goto err

:err
echo [warn] Path update did not complete! Paths will be broken. Run `pathupdate' to fix. 1>&2
goto quit

:done
echo [info] Success.
goto quit

:quit
exit /B