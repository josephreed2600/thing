find \opt -maxdepth 3 -iregex "\\opt\\[^\\]+\(\\bin\)?\\[^\\]+\.exe" 1>\var\cache\paths
for /F "usebackq" %%A in (`cat \var\cache\paths`) do @(
if not exist "\opt\bin\%%~nxA" mklink "\opt\bin\%%~nxA" "%%A"
)