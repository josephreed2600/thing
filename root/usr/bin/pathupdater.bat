@echo off
pushd .
set pu_v=0
set duplicate=
set debug=REM
if "%1"=="-v" set pu_v=1
if "%1"=="--verbose" set pu_v=1
if "%pu_v%"=="1" set duplicate=^| tee con
if "%pu_v%"=="1" set debug=echo [debug]

echo Please wait while system path lists are populated. This may take several minutes.
set pathupdate=1

%debug% Switching to / (%systemdrive%)
%systemdrive%
REM find \ -iregex "^\(\\[^\\]+\)*\\\(s?bin\|lib\)" -maxdepth 4 -type d

REM 2017-11-08 Removed /s?/ from patterns, as sbin shouldn't be included in standard user's path
%debug% Searching for /bin^|lib/ up to depth 4 in /
%systemdrive%\bin\find \ -iregex ".*\\\(bin\|lib\)$" -maxdepth 4 -type d 2>nul %duplicate% | %systemdrive%\usr\bin\awk "{ print length, $0 }" | %systemdrive%\usr\bin\sort -n | %systemdrive%\usr\bin\cut -d" " -f2- | %systemdrive%\usr\bin\dos2unix | %systemdrive%\bin\sed -r "s/^/%SYSTEMDRIVE%/" | %systemdrive%\usr\bin\dos2unix | %systemdrive%\usr\bin\tr \n ; > %systemdrive%\var\cache\paths

%debug% Switching to /opt (%programfiles%)
%programfiles%
%debug% Searching for all surface-level directories in /opt/
%systemdrive%\bin\find \ -maxdepth 1 -type d 2>nul %duplicate% | %systemdrive%\usr\bin\awk "{ print length, $0 }" | %systemdrive%\usr\bin\sort -n | %systemdrive%\usr\bin\cut -d" " -f2- | %systemdrive%\usr\bin\dos2unix | %systemdrive%\bin\sed -r "s/^/%PROGRAMFILES%/" | %systemdrive%\usr\bin\dos2unix | %systemdrive%\usr\bin\tr \n ; > %systemdrive%\var\cache\optpaths

%debug% Searching for subdirectories matching /bin^|lib/ up to depth 2 in /opt/*/
%systemdrive%\bin\find \ -iregex ".*\\\(bin\|lib\)$" -maxdepth 2 -type d 2>nul %duplicate% | %systemdrive%\usr\bin\awk "{ print length, $0 }" | %systemdrive%\usr\bin\sort -n | %systemdrive%\usr\bin\cut -d" " -f2- | %systemdrive%\usr\bin\dos2unix | %systemdrive%\bin\sed -r "s/^/%PROGRAMFILES%/" | %systemdrive%\usr\bin\dos2unix | %systemdrive%\usr\bin\tr \n ; >> %systemdrive%\var\cache\optpaths


%systemdrive%
popd
set pathupdate=0
exit /b %pathupdate%