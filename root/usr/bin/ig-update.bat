
mkdir %USERPROFILE%\media\i\ig\%1
pushd %USERPROFILE%\media\i\ig\%1
\bin\curl -b cookies -c cookies https://www.instagram.com/%1/ | tr -d \n | sed -r "s/<(style|head)>.+\/(\1)?>//g;s/>/>\n/g" | grep -Ee "^window._sharedData" | sed -r "s/<\/script>|window\._sharedData = //g" | tr ;, \n | grep -ie "display_src" | sed -r "s/^.+(http.+\.jpg).+$/\1/g" | xargs -i"URL" curl -O "URL"
popd