@echo off

set aria2c=aria2c.exe
if "%1"=="--help" goto help
if "%1"=="-h" goto help
if "%1"=="" goto help

REM goto findbin
:detectops
if "%1"=="--version" goto version
if "%1"=="-v" goto version
REM if "%2"=="" goto help		# commented because mirrors would mean only one parameter
if exist "%1" set mirrors="%1"
if not defined mirrors goto dl1
if defined mirrors goto dlm

REM :findbin
REM which %aria2c% > %tmp%\%0.which
REM for /F "usebackq delims=" %%A in (`cat %tmp%\%0.which`) do set 
REM goto detectops

:dl1
set dlt=dl1
if "%2"=="" (
set dlsplit=16
) else (
set dlsplit=%2
)
echo Downloading %1 through %dlsplit% connections. Continuing if applicable.
%aria2c% %1 --file-allocation=none -c -s %dlsplit% -x %dlsplit% -j %dlsplit% %3 %4 %5 %6 %7 %8 %9
if not "%errorlevel%"=="0" goto error
goto end

:dlm
set dlt=dlm
echo Downloading from list of mirrors in %mirrors%. Continuing if applicable.
%aria2c% -i %mirrors% --file-allocation=none -c %2 %3 %4 %5 %6 %7 %8 %9
if not "%errorlevel%"=="0" goto error
goto end

:help
echo Usage:
echo arianna ^<URL^> n
echo arianna ^<mirrors.txt^>
echo arianna --help ^| -h
echo arianna --version ^| -v
echo.
echo 	^<URL^>		URL of the file to be downloaded
echo 	n		Maximum number of connections to use
echo 	^<mirrors.txt^>	Path and name of a text file containing
echo 			a list of mirrors to the same file. URIs must
echo 			be separated with a tab space (`\t'), I believe.
echo.
echo    -h	--help		Show this help information and exit.
echo    -v	--version	Show version information and exit.
echo.
goto end

:version
echo arianna downloader 1.5
echo frontend for aria2c
which %aria2c% 1> %tmp%\%0.which 2> %tmp%\%0.not.which
cat %tmp%\%0.which %tmp%\%0.not.which | sed -r "s/^which: no .+$/[WARN] Required binary `%aria2c%\' could not be located. Try changing `\x25aria2c\x25\' in this script./"
echo.
%aria2c% --version
echo.
echo Run arianna without options to display help.
echo.
goto end

:error
echo.
color
echo An error has occurred, and aria2c exited unexpectedly.
choice /C YN /T 300 /D Y /M "Would you like to retry the download? "
if %errorlevel%==2 goto end
if %errorlevel%==1 goto %dlt%

:end