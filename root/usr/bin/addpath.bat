REM @echo off
REM SETX PATH "%PATH%;%1" /M

if "%1"=="" (goto h)

echo %~dpf1

set t=%~dpf1
set bin=%t%\bin
set lib=%t%\lib

if exist "%bin%" (echo foo) else (echo bar)

REM if exist "%bin%" (
REM echo  echo %bin%
REM echo  echo %bin%;^>^>%systemdrive%\var\cache\paths
REM   path %bin%;%path%
REM   if exist "%lib%" (
REM     echo %lib%
REM     echo %lib%;>>%systemdrive%\var\cache\paths
REM     path %lib%;%path%
REM   )
REM ) else (
echo 1
echo %t%
  if exist "%t%" (
echo 2
    echo %t%
echo 3
    echo %t%;>>%systemdrive%\var\cache\paths
echo 4
    path %t%;%path%
echo 5
  ) else (
    goto err
  )
REM )

cat %systemdrive%\var\cache\paths | tr -d \n\r 1> %systemdrive%\var\tmp\paths
rm %systemdrive%\var\cache\paths
mv %systemdrive%\var\tmp\paths %systemdrive%\var\cache

:exit
exit /b



:err
echo %0: path "%~dpf1" not found1>&2
goto exit

:h
echo usage:
echo addpath ^<path^>
echo.
echo 	^<path^> may be absolute or relative. It will be resolved
echo 		into an absolute path automatically, and necessary
echo 		subdirectories (bin and lib) will be added if they
echo 		exist.