@echo off
echo WINIT: version 1.02 booting

%~d0

REM pushd \etc

for /F "usebackq delims=: tokens=1,2,3,4" %%A in (`\bin\cat \etc\inittab ^| \bin\grep -v ^^#`) do @if "%%C"=="initdefault" @set runlevel=%%B

REM Copied into \sbin\telinit.cmd, update there too
for /F "usebackq delims=: tokens=1,2,3,4" %%A in (`\bin\cat \etc\inittab ^| \bin\grep -v ^^#`) do @if "%%B"=="%runlevel%" @%%D

REM Moved to /etc/init.d/rc.bat
REM for /F "usebackq delims=" %%A in (`\bin\ls -1 rc%runlevel%.d`) do "\etc\rc%runlevel%.d\%%A"

REM The following can't be in the right place lol
REM call \usr\bin\aliases
REM set USERNAME=sir
REM call \etc\init.d\loaduser.bat
