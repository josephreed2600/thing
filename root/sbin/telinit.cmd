@echo off
echo [info] Switching runlevels from %runlevel% to %1
set prevlevel=%runlevel%
set runlevel=%1

\etc\init.d\rc %prevlevel% k

REM Copied from \sbin\init.cmd, update there too
for /F "usebackq delims=: tokens=1,2,3,4" %%A in (`\bin\cat \etc\inittab ^| \bin\grep -v ^^#`) do @if "%%B"=="%runlevel%" @%%D
