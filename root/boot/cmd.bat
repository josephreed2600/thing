REM @echo off
%~d0
pushd \boot

set SystemDrive=%~d0
set SystemRoot=%~d0

for /F "usebackq tokens=1,2" %%A in (\etc\fstab) do @call \bin\mount-guid %%A %%B

set HOMEDRIVE=%home%
set PUBLIC=%HOMEDRIVE%\home\public
set OLDTMP=%TEMP%
set TEMP=%SystemDrive%\tmp
set TMP=%TEMP%
set ALLUSERSPROFILE=%SystemDrive%\etc
set ProgramFiles=%opt%
set ProgramFiles(x86)=%opt%
set ProgramW6432=%opt%
set CommonProgramFiles=%opt%\common
set CommonProgramFiles(x86)=%opt%\common
set CommonProgramW6432=%opt%\common
set ProgramData=%SystemDrive%\var\opt
set pathext=%pathext%;.LNK

set JAVA_HOME=%opt%\jdk-8u111-windows-amd64
set PYTHONHOME=%opt%\python27
for /F %%A in (\etc\init.d\pythonpath.path) do set PYTHONPATH=%%A
set TESSDATA_PREFIX=%ProgramFiles%\TesseractOCR

REM for /F %%A in (\etc\init.d\custom.path) do set custompath=%%A
REM path %custompath%;%path%
REM call custompaths

REM Add custom paths below
set path=%PYTHONHOME%\Scripts;%path%

@echo on

set pathbak=%path%
path %SYSTEMDRIVE%\bin;%SYSTEMDRIVE%\sbin;%SYSTEMDRIVE%\usr\bin;%SYSTEMDRIVE%\usr\sbin;%opt%\bin;%SYSTEMDRIVE%\lib;%SYSTEMDRIVE%\usr\lib;%path%

if exist \var\cache\optpaths (
 cat \var\cache\optpaths | tr ; \n | sed "s#^.:\\#%PROGRAMFILES%\\#" | dos2unix | tr \n ; > \var\cache\optpaths.tmp
 del \var\cache\optpaths
 rename \var\cache\optpaths.tmp optpaths
) else (call \usr\bin\pathupdater)

if exist \var\cache\paths (
 cat \var\cache\paths | tr ; \n | sed "s#^.:\\#%SYSTEMDRIVE%\\#" | dos2unix | tr \n ; > \var\cache\paths.tmp
 del \var\cache\paths
 rename \var\cache\paths.tmp paths
) else (call \usr\bin\pathupdater)

for /F "usebackq delims=" %%A in (`\bin\cat \var\cache\optpaths`) do set optpath=%%A
for /F "usebackq delims=" %%A in (`\bin\cat \var\cache\paths`) do set rootpath=%%A
set path=%rootpath%%optpath%%pathbak%
REM set path=bin;..\bin;lib;..\lib;%path%%pathbak%

call \usr\bin\aliases

set USERNAME=sir
call \etc\init.d\loaduser
REM exit /b