`/opt`
======

Install optional software packages under this directory. Each package should have its own directory. The path-generation scripts will search a certain depth (four levels?) down into `/opt/` and add any folder with a binary or library, if I recall correctly.
