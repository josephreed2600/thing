@echo off
REM color 0A

REM Add aliases below
doskey nano=notepad $*
doskey gedit=notepad $*

set ECLIPSE_HOME=%opt%\eclipse
set ECLIPSEHOME=%ECLIPSE_HOME%
set ECLIPSE.HOME=%ECLIPSE_HOME%
set USER_HOME=%HOMEPATH%
set USERHOME=%USER_HOME%
set USER.HOME=%USER_HOME%