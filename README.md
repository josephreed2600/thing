Reed's Monster
==============

History
-------

There was a time when I exclusively used Windows. When I discovered Linux, I was amazed by the atmosphere of uniformity, organization, and standardization that permeated the system. I enjoyed learning my way around the filesystem and seeing how everything was arranged. But I still used Windows for all my "serious" work. Linux was just a toy, confined to live CDs.

I didn't have the resources to support dualbooting, and I was far too inexperienced for making a full transition to be wise. (I still keep Windows around somewhere just in case I need it.) But I was hooked; the love affair had begun. Being the glutton for punishment that I am, rather than focus on gaining actual Linux experience, I set about my favorite pastime: Doing Things the Wrong Way. I set about researching the Filesystem Hierarchy Standard. I started consolidating data from my numerous flash drives into a more organized system, spanning two drives: `/` and `/home`.

Organizing my current data was only the first step. After that, I decided I wanted a shell environment (on Windows) that would run some scripts on startup to set up my preferences, set path variables, and so on. Thus `.cmdrc.cmd` was born. If I could organize my data, why not keep portable software organized and accessible, too? Soon, I had a third drive for `/opt`. There were difficulties in installing software with non-standard environment variables. Some installers refuse to accept that `%ProgramFiles%` can be anything other than `C:\Program Files`. Some of these challenges were overcome; some have yet to be mastered.

With `/opt` came the issue of the path variable. I explored links, symbolic links, hard links, shortcuts, but nothing seemed sufficient for creating a pool of links to all the binaries for installed software. Why not clutter the path variable with every directory that has a major program in it? (Don't answer that.) Functionality was added to the shell startup scripts to automatically generate (long process) or modify (shorter process) such a path variable. Things got cluttered, so they were reorganized.

At some point it became evident that a system for "mounting" other drives was necessary. The `.partid` file in each of the partitions is used to identify which partition is which, since Windows cannot reliably assign the same drive letter to a drive on any machine. (A user could recombine the partitions into one, or create a different partitioning scheme, in which case the user might want to visit `/etc/fstab` to make the appropriate adjustments.)

Finally, as university began to close in on my time, I started working on init scripts. Eventually, a user will be prompted to log in, a password will be checked, and other stuff will be done. The path variable, barring a better solution, will be automatically updated whenever the contents of `/opt` change. `/tmp` will be cleared when the last user logs off (apparently user sessions will be tracked to manage this, which sounds like a thing that's probably done). Over time, with development, the project will grow more and more similar in function to a Unix system, all within a Windows `cmd` shell.

Of course, this is a ridiculous project exhibiting some hideous practices, and I should be chastised for creating it.

Practical Notes
---------------

Launch the system using `/do` (the old init system) or `/i` (the new init system, in progress).

The contents of `/opt` have been gutted, because they were a bunch of programs that (a) were not integral to the concept of this project and (b) I probably didn't have permission to release here anyway. Most of the contents of `/home/sir` have also been removed, but some directories still exist (`AppData` is important and still used by applications). When an application is installed in `/opt`, the path must be regenerated with some tool, probably in `/usr/bin/` or something. Good luck. Launch programs from the command line so they will inherit the modified environment variables. Note that some systems can break entirely when certain variables are modified. (If you find out what causes the something something DNS authoritative something error that makes everything stop working, please file an issue [and then a PR]!)
